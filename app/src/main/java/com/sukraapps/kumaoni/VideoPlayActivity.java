package com.sukraapps.kumaoni;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.sukraapps.kumaoni.adapter.ClickListener;
import com.sukraapps.kumaoni.adapter.CommentAdapter;
import com.sukraapps.kumaoni.adapter.RecyclerTouchListener;
import com.sukraapps.kumaoni.adapter.VideoAdapter;
import com.sukraapps.kumaoni.model.Comment;
import com.sukraapps.kumaoni.model.Favourite;
import com.sukraapps.kumaoni.model.Video;
import com.sukraapps.kumaoni.service.ApiClient;
import com.sukraapps.kumaoni.service.ApiService;
import com.sukraapps.kumaoni.util.Util;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoPlayActivity extends YouTubeBaseActivity implements SwipeRefreshLayout.OnRefreshListener, YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_DIALOG_REQUEST = 1;

    RecyclerView recyclerView;
    RecyclerView recyclerViewComment;

    ArrayList<Video> arrVideoList;
    Video video;
    int offset = 0, vidPos = 0;

    RelativeLayout relHeader;
    Button btnBack;
    TextView textHeader;
    ImageView favourite;
    String developer_key;

    private MyPlaybackEventListener playbackEventListener;
    private YouTubePlayerView youTubeView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        developer_key = Util.getYoutubeApiKey(this);
        try{
            if (getIntent().getExtras() != null) {
                video = (Video) getIntent().getSerializableExtra("Video");
                offset = getIntent().getIntExtra("offset", 0);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"Video not found",Toast.LENGTH_SHORT).show();
            finish();
        }

        setUpUi();

        Log.i("YouTueb API Key",developer_key);

        textHeader.setText(video.getTitle());
        /*String plain = Html.fromHtml(video.getDescription()).toString();
        WebView webViewIngredients = (WebView) findViewById(R.id.webViewIngredients);
        webViewIngredients.getSettings().setJavaScriptEnabled(true);
        webViewIngredients.loadData(plain, "text/html; charset=UTF-8", null);*/

        setRecyclerview();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Video v = arrVideoList.get(position);
                Intent iv = new Intent(getApplicationContext(), VideoPlayActivity.class);
                iv.putExtra("Video", v);
                iv.putExtra("offset", offset);
                startActivity(iv);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFavouriteVideo();
            }
        });

        findViewById(R.id.share_IB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareVideo();
            }
        });

        setUpComments();
    }

    private void setUpUi() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        relHeader = (RelativeLayout) findViewById(R.id.relHeader);
        favourite = (ImageView) findViewById(R.id.favourite_IB);
        textHeader = (TextView) findViewById(R.id.txtHeader);
        btnBack = (Button) findViewById(R.id.btnBack);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(developer_key, this);

        playbackEventListener = new MyPlaybackEventListener();


        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        arrVideoList = new ArrayList<Video>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new com.sukraapps.kumaoni.adapter.DividerItemDecoration(this, com.sukraapps.kumaoni.adapter.DividerItemDecoration.HORIZONTAL_LIST));

        findViewById(R.id.comment_BTN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });
    }

    private void setRecyclerview() {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<Favourite> call = apiService.getSuggestedVideos(Util.getUserId(this),video.getId(), video.getSubCategoryId(), getPackageName());
        call.enqueue(new Callback<Favourite>() {
            @Override
            public void onResponse(Call<Favourite> call, Response<Favourite> response) {
                try{
                    if (response.body() != null) {
                        arrVideoList = response.body().getVideos();

                        if (response.body().isFavourite()){
                            favourite.setImageDrawable(getDrawable(R.drawable.ic_favorite_white_24dp));
                        }else{
                            favourite.setImageDrawable(getDrawable(R.drawable.ic_favorite_border_black_24dp));
                        }

                        VideoAdapter adapter = new VideoAdapter(getApplicationContext(), arrVideoList);
                        recyclerView.setAdapter(adapter);
                    }
                }catch (Exception e){}
            }

            @Override
            public void onFailure(Call<Favourite> call, Throwable t) {
                Toast.makeText(getApplicationContext(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setFavouriteVideo(){
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<Favourite> call = apiService.setFavouriteVideo(Util.getUserId(this),video.getId(), getPackageName());
        call.enqueue(new Callback<Favourite>() {
            @Override
            public void onResponse(Call<Favourite> call, Response<Favourite> response) {
                try{
                    if (response.body() != null){
                        Favourite favourite1 = response.body();
                        if (favourite1.isFavourite()){
                            favourite.setImageDrawable(getDrawable(R.drawable.ic_favorite_white_24dp));
                        }else{
                            favourite.setImageDrawable(getDrawable(R.drawable.ic_favorite_border_black_24dp));
                        }
                    }
                    findViewById(R.id.progressBar).setVisibility(View.GONE);
                }catch (Exception e){}
            }

            @Override
            public void onFailure(Call<Favourite> call, Throwable t) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                Toast.makeText(VideoPlayActivity.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpComments(){
        recyclerViewComment = (RecyclerView) findViewById(R.id.recyclerviewComment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewComment.setLayoutManager(layoutManager);

        String userid = Util.getUserId(getApplicationContext());
        String videoid = video.getId();

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<ArrayList<Comment>> call = apiService.getComments(userid, videoid);
        call.enqueue(new Callback<ArrayList<Comment>>() {
            @Override
            public void onResponse(Call<ArrayList<Comment>> call, Response<ArrayList<Comment>> response) {
                try{
                    if (response.body() != null){
                        CommentAdapter adapter = new CommentAdapter(getApplicationContext(),response.body());
                        recyclerViewComment.setAdapter(adapter);
                    }
                    findViewById(R.id.commentLinear).setVisibility(View.VISIBLE);
                }catch (Exception e){}
            }

            @Override
            public void onFailure(Call<ArrayList<Comment>> call, Throwable t) {
                findViewById(R.id.commentLinear).setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendComment() {
        String userid = Util.getUserId(getApplicationContext());
        String videoid = video.getId();
        final String comment = ((EditText)findViewById(R.id.comment_ET)).getText().toString();
        if (comment.isEmpty()){
            Toast.makeText(getApplicationContext(),"Please write something to add comment",Toast.LENGTH_SHORT).show();
            return;
        }
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<String> call = apiService.addComment(userid, comment, videoid);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    ((EditText)findViewById(R.id.comment_ET)).setText("");
                    ((CommentAdapter)recyclerViewComment.getAdapter()).add(new Comment(comment,new Date()));
                }catch (Exception e){}
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void shareVideo(){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_title));
        String sAux = "\n"+video.getVideoLink()+"\n\n";
        sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();
        i.putExtra(Intent.EXTRA_TEXT, sAux);
        startActivity(i);
    }


    @Override
    public void onRefresh() {
        setRecyclerview();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {

        if (!b) {
            youTubePlayer.setPlaybackEventListener(playbackEventListener);

            youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onLoaded(String s) {
                    youTubePlayer.play();
                }

                @Override
                public void onAdStarted() {

                }

                @Override
                public void onVideoStarted() {

                }

                @Override
                public void onVideoEnded() {
                    try {
                        if (video.getId().equals(arrVideoList.get(vidPos).getId())) {
                            vidPos++;
                            youTubePlayer.cueVideo(arrVideoList.get(vidPos).getVideoCode());
                            vidPos++;
                        } else {
                            youTubePlayer.cueVideo(arrVideoList.get(vidPos).getVideoCode());
                            vidPos++;
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onError(YouTubePlayer.ErrorReason errorReason) {
                    String error = "";
                    if (errorReason == YouTubePlayer.ErrorReason.AUTOPLAY_DISABLED){
                        error = "AUTOPLAY_DISABLED";
                    }else if (errorReason == YouTubePlayer.ErrorReason.BLOCKED_FOR_APP){
                        error = "BLOCKED_FOR_APP";
                    }else if (errorReason == YouTubePlayer.ErrorReason.NOT_PLAYABLE){
                        error = "NOT_PLAYABLE";
                    }else if (errorReason == YouTubePlayer.ErrorReason.NETWORK_ERROR){
                        error = "NETWORK_ERROR";
                    }else if (errorReason == YouTubePlayer.ErrorReason.UNAUTHORIZED_OVERLAY){
                        error = "UNAUTHORIZED_OVERLAY";
                    }else if (errorReason == YouTubePlayer.ErrorReason.PLAYER_VIEW_TOO_SMALL){
                        error = "PLAYER_VIEW_TOO_SMALL";
                    }else if (errorReason == YouTubePlayer.ErrorReason.PLAYER_VIEW_NOT_VISIBLE){
                        error = "PLAYER_VIEW_NOT_VISIBLE";
                    }else if (errorReason == YouTubePlayer.ErrorReason.EMPTY_PLAYLIST){
                        error = "EMPTY_PLAYLIST";
                    }else if (errorReason == YouTubePlayer.ErrorReason.AUTOPLAY_DISABLED){
                        error = "AUTOPLAY_DISABLED";
                    }else if (errorReason == YouTubePlayer.ErrorReason.USER_DECLINED_RESTRICTED_CONTENT){
                        error = "USER_DECLINED_RESTRICTED_CONTENT";
                    }else if (errorReason == YouTubePlayer.ErrorReason.USER_DECLINED_HIGH_BANDWIDTH){
                        error = "USER_DECLINED_HIGH_BANDWIDTH";
                    }else if (errorReason == YouTubePlayer.ErrorReason.UNEXPECTED_SERVICE_DISCONNECTION){
                        error = "UNEXPECTED_SERVICE_DISCONNECTION";
                    }else if (errorReason == YouTubePlayer.ErrorReason.INTERNAL_ERROR){
                        error = "INTERNAL_ERROR";
                    }else if (errorReason == YouTubePlayer.ErrorReason.UNKNOWN){
                        error = "UNKNOWN";
                    }else{
                        error = "unknown_error";
                    }

                    String vidId = "0";
                    try{
                        vidId = arrVideoList.get(vidPos-1).getId();
                    }catch (Exception e){
                        vidId = video.getId();
                    }
                    updateErrorToDb(error,vidId);

                    try {
                        if (video.getId().equals(arrVideoList.get(vidPos).getId())) {
                            vidPos++;
                            youTubePlayer.cueVideo(arrVideoList.get(vidPos).getVideoCode());
                            vidPos++;
                        } else {
                            youTubePlayer.cueVideo(arrVideoList.get(vidPos).getVideoCode());
                            vidPos++;
                        }
                    } catch (Exception e) {
                    }
                }
            });

            youTubePlayer.cueVideo(video.getVideoCode()); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "There was an error initializing youtube player", youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }

        String vidId = "0";
        try{
            vidId = arrVideoList.get(vidPos-1).getId();
        }catch (Exception e){
            vidId = video.getId();
        }
        updateErrorToDb("Player_Initialization_Error",vidId);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(developer_key, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    public void viewBackClick(View view) {
        finish();
    }


    private class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {
        @Override
        public void onPlaying() {

            relHeader.setVisibility(View.GONE);
        }

        @Override
        public void onPaused() {

            relHeader.setVisibility(View.VISIBLE);
        }

        @Override
        public void onStopped() {
        }

        @Override
        public void onBuffering(boolean b) {
        }

        @Override
        public void onSeekTo(int i) {
        }
    }

    private void updateErrorToDb(String reason,String videoId){
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<String> call = apiService.updateVideoError(reason,videoId,Util.getUserId(this));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}