package com.sukraapps.kumaoni.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sukraapps.kumaoni.R;
import com.sukraapps.kumaoni.model.Video;

import java.util.ArrayList;

/**
 * Created by Ravi on 13-Sep-17.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

    Context context;
    ArrayList<Video> videoArrayList;

    public VideoAdapter(Context context, ArrayList<Video> videoArrayList) {
        this.context = context;
        this.videoArrayList = videoArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Video video = videoArrayList.get(position);

        try{
            if (video != null){
                /*if (video.getIs_audio() == 1){
                    holder.audioImageView.setVisibility(View.VISIBLE);
                    holder.audioBack.setVisibility(View.VISIBLE);
                }*/
                holder.title.setText(video.getTitle());
                holder.views.setText(video.getViews()+" Views");
                //holder.title.setTypeface(Util.getTypeface(context, Util.MUKTAVANI_SEMIBOLD));
                String imgUrl = "http://img.youtube.com/vi/" + video.getVideoCode() + "/0.jpg";
                Glide.with(context).load(imgUrl).thumbnail(0.5f).crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageView);
            }
        }catch (Exception e){}


    }

    public void add(ArrayList<Video> videos){
        videoArrayList.addAll(videos);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return videoArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView views;
        ImageView imageView;
        TextView audioImageView;
        View audioBack;

        public MyViewHolder(View itemView) {
            super(itemView);

           /* date = (TextView) itemView.findViewById(R.id.date_TV);*/
            title = (TextView) itemView.findViewById(R.id.title);
            views = (TextView) itemView.findViewById(R.id.views);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            audioImageView = (TextView) itemView.findViewById(R.id.audioIV);
            audioBack = itemView.findViewById(R.id.audioBack);
        }
    }
}