package com.sukraapps.kumaoni.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sukraapps.kumaoni.R;
import com.sukraapps.kumaoni.model.Comment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Ravi on 07-Oct-17.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {

    Context context;
    ArrayList<Comment> commentArrayList;

    public CommentAdapter(Context context, ArrayList<Comment> videoArrayList) {
        this.context = context;
        this.commentArrayList = videoArrayList;
    }

    @Override
    public CommentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);

        return new CommentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentAdapter.MyViewHolder holder, int position) {
        Comment comment = commentArrayList.get(position);

        try{
            holder.title.setText(comment.getComment());
            holder.time.setText(new SimpleDateFormat("d/M/yy h:mm:ss a").format(comment.getDatetime()));
        }catch (Exception e){}

    }

    public void add(Comment comment){
        commentArrayList.add(comment);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return commentArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView time;

        public MyViewHolder(View itemView) {
            super(itemView);

           /* date = (TextView) itemView.findViewById(R.id.date_TV);*/
            title = (TextView) itemView.findViewById(R.id.comment);
            time = (TextView) itemView.findViewById(R.id.time);
        }
    }
}