package com.sukraapps.kumaoni.adapter;

/**
 * Created by Ravi on 04-Oct-17.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;


import com.sukraapps.kumaoni.R;
import com.sukraapps.kumaoni.model.Category;
import com.sukraapps.kumaoni.model.SubCategory;
import com.sukraapps.kumaoni.util.Util;

import java.util.ArrayList;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    ArrayList<Category> categoryArrayList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public CustomExpandableListAdapter(Context context, ArrayList<Category> categoryArrayList) {
        mContext = context;
        this.categoryArrayList = categoryArrayList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return categoryArrayList.get(listPosition).getSubCategories().get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        SubCategory expandedListText = categoryArrayList.get(listPosition).getSubCategories().get(expandedListPosition);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        expandedListTextView.setText(expandedListText.getTitle());
        expandedListTextView.setTypeface(Util.getTypeface(mContext, Util.MUKTAVANI_SEMIBOLD));

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return categoryArrayList.get(listPosition).getSubCategories().size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return categoryArrayList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return categoryArrayList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(final int listPosition, final boolean isExpanded,
                             View convertView, final ViewGroup parent) {
        Category category = categoryArrayList.get(listPosition);

        convertView = mLayoutInflater.inflate(R.layout.list_group, null);

        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);

        listTitleTextView.setTypeface(Util.getTypeface(mContext, Util.MUKTAVANI_BOLD));
        listTitleTextView.setText(category.getTitle());


        if (category.getId() < 1000) {
            ImageView listHeaderArrow = (ImageView) convertView.findViewById(R.id.left_menu_list_header_arrow);

            //set drawable in Expandablelistview
            int imageResourceId = isExpanded ? R.drawable.ic_keyboard_arrow_up_black_24dp : R.drawable.ic_keyboard_arrow_down_black_24dp;
            listHeaderArrow.setImageResource(imageResourceId);
            listHeaderArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int pos = listPosition;
                    //if Group is expanded then collapse group
                    if (isExpanded) {
                        ((ExpandableListView) parent).collapseGroup(pos);
                    }
                    //else group is collapsed then expand group
                    else {
                        ((ExpandableListView) parent).expandGroup(pos, true);
                    }
                }
            });
            listHeaderArrow.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}