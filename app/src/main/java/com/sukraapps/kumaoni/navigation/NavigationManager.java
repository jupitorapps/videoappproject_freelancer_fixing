package com.sukraapps.kumaoni.navigation;


public interface NavigationManager {

    void defaultFragment(int id, boolean isCategory);

    void favouriteFragment();
}