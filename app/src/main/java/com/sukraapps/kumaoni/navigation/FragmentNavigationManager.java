package com.sukraapps.kumaoni.navigation;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.sukraapps.kumaoni.BuildConfig;
import com.sukraapps.kumaoni.MainActivity;
import com.sukraapps.kumaoni.R;
import com.sukraapps.kumaoni.fragment.FavouriteFragment;
import com.sukraapps.kumaoni.fragment.HomeFragment;


public class FragmentNavigationManager implements NavigationManager {

    private static FragmentNavigationManager sInstance;

    private FragmentManager mFragmentManager;
    private MainActivity mActivity;

    public static FragmentNavigationManager obtain(MainActivity activity) {
        if (sInstance == null) {
            sInstance = new FragmentNavigationManager();
        }
        sInstance.configure(activity);
        return sInstance;
    }

    private void configure(MainActivity activity) {
        mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
    }

    @Override
    public void defaultFragment(int id, boolean isCategory) {
        showFragment(HomeFragment.newInstance(id,isCategory), false);
    }

    @Override
    public void favouriteFragment() {
        showFragment(FavouriteFragment.newInstance(), false);
    }

    private void showFragment(Fragment fragment, boolean allowStateLoss) {
        FragmentManager fm = mFragmentManager;

        @SuppressLint("CommitTransaction")
        FragmentTransaction ft = fm.beginTransaction().replace(R.id.container, fragment, "MyFragment");

        ft.addToBackStack(null);

        if (allowStateLoss || !BuildConfig.DEBUG) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }

        fm.executePendingTransactions();
    }
}
