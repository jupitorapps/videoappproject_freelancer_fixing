package com.sukraapps.kumaoni.service;

import com.sukraapps.kumaoni.model.Ad;
import com.sukraapps.kumaoni.model.Category;
import com.sukraapps.kumaoni.model.Comment;
import com.sukraapps.kumaoni.model.Favourite;
import com.sukraapps.kumaoni.model.Setting;
import com.sukraapps.kumaoni.model.Video;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ravi on 11-Sep-17.
 */

public interface ApiService {

    String name = "devaguru_kumaonivideo";

    @GET("getCategories.php?dbname="+name)
    Call<ArrayList<Category>> getCategories();

    @GET("getHomeVideos.php?dbname="+name)
    Call<ArrayList<Video>> getHomeVideos(@Query("userid") String userid,@Query("offset") int offset,@Query("filter") String filter);

    @GET("getHomeVideosByCategoryId.php?dbname="+name)
    Call<ArrayList<Video>> getHomeVideosByCategoryId(@Query("offset") int offset,@Query("categoryid") int categoryid,@Query("filter") String filter);

    @GET("getHomeVideosBySubCategoryId.php?dbname="+name)
    Call<ArrayList<Video>> getHomeVideosBySubCategoryId(@Query("offset") int offset,@Query("subcategoryid") int subcategoryid,@Query("filter") String filter);

    @GET("getSuggestedVideos2.php?dbname="+name)
    Call<Favourite> getSuggestedVideos(@Query("userid") String userid,@Query("videoid") String videoid,@Query("categoryid") int categoryid, @Query("package") String packagename);

    @GET("getHDVideos.php?dbname="+name)
    Call<ArrayList<Video>> getHDVideos(@Query("offset") int offset);

    @GET("getTrendingVideos.php?dbname="+name)
    Call<ArrayList<Video>> getTrendingVideos(@Query("offset") int offset,@Query("filter") String filter);

    @GET("getLatestVideos.php?dbname="+name)
    Call<ArrayList<Video>> getLatestVideos(@Query("offset") int offset,@Query("filter") String filter);

    @GET("getHistoryVideos2.php?dbname="+name)
    Call<ArrayList<Video>> getHistoryVideos(@Query("userid") String userid ,@Query("offset") int offset, @Query("package") String packagename);

    @GET("getSearchVideos.php?dbname="+name)
    Call<ArrayList<Video>> getSearchVideos(@Query("userid") String userid ,@Query("keyword") String keyword ,@Query("offset") int offset);

    @GET("setFavouriteVideo2.php?dbname="+name)
    Call<Favourite> setFavouriteVideo(@Query("userid") String userid, @Query("videoid") String videoid, @Query("package") String packagename);

    @GET("getFavouriteVideos2.php?dbname="+name)
    Call<ArrayList<Video>> getFavouriteVideos(@Query("userid") String userid, @Query("package") String packagename);

    @GET("getUserId.php?dbname="+name)
    Call<String> getUserId(@Query("deviceid") String deviceid);

    @GET("getSettings.php?dbname="+name)
    Call<Setting> getSettings(@Query("userid") String userid,@Query("count") int count);

    @GET("insertUserVideo.php?dbname="+name)
    Call<String> insertUserVideo(@Query("userid") String userid ,@Query("videoid") String videoid,@Query("catid") String catid);

    @GET("addComment.php?dbname="+name)
    Call<String> addComment(@Query("userid") String userid ,@Query("comment") String comment,@Query("videoid") String videoid);

    @GET("getComments.php?dbname="+name)
    Call<ArrayList<Comment>> getComments(@Query("userid") String userid , @Query("videoid") String videoid);

    @GET("saveReview.php?dbname="+name)
    Call<String> saveReview(@Query("userid") String userid , @Query("review") String review);

    @GET("updateOnServerThatUserMadeAdFree.php?dbname="+name)
    Call<String> updateOnServerThatUserMadeAdFree(@Query("userid") String userid);

    @GET("updateVideoError.php?dbname="+name)
    Call<String> updateVideoError(@Query("reason") String reason,@Query("videoid") String videoid,@Query("userid") String userid);

    @GET("getNotificationVideo.php?dbname="+name)
    Call<Video> getNotificationVideo(@Query("userid") String userid);


    @GET("getAdsForVideoApp.php")
    Call<Ad> getAds(@Query("uid") String userid, @Query("package") String packagename);

    @GET("updateAdsSeen.php")
    Call<ResponseBody> updateAdsSeen(@Query("uid") String userid, @Query("package") String packagename,
                                     @Query("adid") String adsid);

    @GET("updateBackAdsSeen.php")
    Call<ResponseBody> updateBackAdsSeen(@Query("uid") String userid, @Query("package") String packagename,
                                         @Query("adid") String adsid);
}
