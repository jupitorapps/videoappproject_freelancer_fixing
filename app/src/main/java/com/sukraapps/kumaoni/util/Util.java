package com.sukraapps.kumaoni.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import com.sukraapps.kumaoni.R;

/**
 * Created by FIRST on 15-09-2017.
 */

public class Util {

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String USER_ID = "userid";
    public static final String VIDEO_SHARE_COUNT = "videoShare";
    public static final String VIDEO_FB_SHARE_COUNT = "videoFbShare";

    public static final String YOUTUBE_API_KEY = "youtubeApi";
    public static final String ADS_VISIBILITY = "adsVisibility";

    public static final String APP_OPEN_COUNT = "appOpencount";
    public static final String IS_APP_RATED = "isAppRated";

    public static final String MUKTAVANI_BOLD = "MuktaVaani-Bold.ttf";
    public static final String MUKTAVANI_MEDIUM = "MuktaVaani-Medium.ttf";
    public static final String MUKTAVANI_SEMIBOLD = "MuktaVaani-SemiBold.ttf";

    public static Typeface getTypeface(Context context, String font) {
        String fontType = "fonts/" + font;
        return Typeface.createFromAsset(context.getAssets(), fontType);
    }

    public static void saveUserId(Context context, String userid ) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_ID,userid);
        editor.apply();
    }

    public static String getUserId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getString(USER_ID,"");
    }

    public static int getAppOpenCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getInt(APP_OPEN_COUNT,1);
    }

    public static void updateAppOpenCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        int count = preferences.getInt(APP_OPEN_COUNT,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(APP_OPEN_COUNT,count+1);
        editor.apply();
    }

    public static String getYoutubeApiKey(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getString(YOUTUBE_API_KEY,context.getResources().getString(R.string.dev_k));
    }

    public static void saveYoutubeApiKey(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(YOUTUBE_API_KEY,key);
        editor.apply();
    }

    public static boolean shouldShowAds(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        int no = preferences.getInt(VIDEO_SHARE_COUNT,10);
        int noFB = preferences.getInt(VIDEO_SHARE_COUNT,1);
        boolean shouldShowAd = true;
        if (no == 0 && noFB == 0){
            shouldShowAd = false;
        }
        if (preferences.getBoolean(ADS_VISIBILITY,true) && shouldShowAd && context.getString(R.string.ADS_VISIBILITY).equals("YES")){
            return true;
        }else{
            return false;
        }
    }

    public static boolean shouldShowRateDialog(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        boolean appRated = preferences.getBoolean(IS_APP_RATED,false);
        if (appRated){
            return false;
        }else{
            int no = preferences.getInt(APP_OPEN_COUNT,1);
            if (no % 3 == 0){
                return true;
            }else{
                return false;
            }
        }
    }

    public static void saveAppRatedToTrue(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_APP_RATED,true);
        editor.apply();
    }

}
