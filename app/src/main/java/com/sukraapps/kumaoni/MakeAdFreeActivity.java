package com.sukraapps.kumaoni;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sukraapps.kumaoni.service.ApiClient;
import com.sukraapps.kumaoni.service.ApiService;
import com.sukraapps.kumaoni.util.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeAdFreeActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    int videoCount = 0;
    int videoCountFB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_ad_free);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Make Ad Free");

        sharedPreferences = getSharedPreferences(Util.SHARED_PREFS,MODE_PRIVATE);

        videoCount = sharedPreferences.getInt(Util.VIDEO_SHARE_COUNT,10);
        videoCountFB = sharedPreferences.getInt(Util.VIDEO_FB_SHARE_COUNT,1);
        ((TextView)findViewById(R.id.remained)).setText("Remained - "+videoCount);
        ((TextView)findViewById(R.id.remainedFB)).setText("Remained - "+videoCountFB);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void shareBtnClick(View view) {
        try {


            String sAux = "\n"+getResources().getString(R.string.share_description)+"\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();

            Bitmap bitmap= BitmapFactory.decodeResource(getResources(),R.drawable.shareimage);
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/LatestShare.jpg";
            OutputStream out = null;
            File file=new File(path);
            try {
                out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            path=file.getPath();
            Uri bmpUri = Uri.parse("file://"+path);


            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_TEXT, sAux);
            intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            intent.setType("image/*");
            intent.setPackage("com.whatsapp");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            boolean whatsAppFound = false;
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo info : matches) {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.whatsapp")) {
                    whatsAppFound = true;
                    break;
                }
            }
            if (!whatsAppFound){
                Toast.makeText(getApplicationContext(),"Whatsapp not found",Toast.LENGTH_LONG).show();
                return;
            }

            startActivityForResult(Intent.createChooser(intent, "Select app"),201);
        } catch (Exception e) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.setPackage("com.whatsapp");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_title));
            String sAux = "\n"+getResources().getString(R.string.share_description)+"\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Select app"));
        }
    }

    public void shareFBBtnClick(View view) {
        String sAux = "\n"+getResources().getString(R.string.share_description)+"\n\n";
        sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,sAux);

        Bitmap bitmap= BitmapFactory.decodeResource(getResources(),R.drawable.shareimage);
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/LatestShare.jpg";
        OutputStream out = null;
        File file=new File(path);
        try {
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        path=file.getPath();
        Uri fileUri = Uri.parse("file://"+path);

        if (fileUri != null) {
            intent.putExtra(Intent.EXTRA_STREAM, fileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/*");
        }

        boolean facebookAppFound = false;
        List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana") ||
                    info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.lite")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

        if (facebookAppFound) {
            startActivityForResult(intent,301);
        } else {
            Toast.makeText(getApplicationContext(),"Facebook app not found",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 201){ // For whats app share
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (videoCount > 0){
                videoCount = videoCount - 1;
                editor.putInt(Util.VIDEO_SHARE_COUNT,videoCount);
                editor.commit();
                ((TextView)findViewById(R.id.remained)).setText("Remained - "+videoCount);
            }
        }
        if (requestCode == 301){  // For Facebook share
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (videoCountFB > 0){
                videoCountFB = videoCountFB - 1;
                editor.putInt(Util.VIDEO_FB_SHARE_COUNT,videoCountFB);
                editor.commit();
                ((TextView)findViewById(R.id.remainedFB)).setText("Remained - "+videoCountFB);
            }
        }

        if(videoCount == 0 && videoCountFB == 0){
            updateOnServerThatUserMadeAdFree();
        }
    }

    private void updateOnServerThatUserMadeAdFree(){
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<String> call = apiService.updateOnServerThatUserMadeAdFree(Util.getUserId(this));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

}
