package com.sukraapps.kumaoni.model;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ravi on 07-Oct-17.
 */

public class Favourite implements Serializable {
    private boolean isFavourite;
    private ArrayList<Video> videos;

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }
}
