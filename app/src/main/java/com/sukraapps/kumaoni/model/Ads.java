package com.sukraapps.kumaoni.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ravi on 04-Jan-18.
 */

public class Ads {
    @SerializedName("id")
    private String id;
    @SerializedName("ad_img_link")
    private String adImgLink;
    @SerializedName("ad_click_link")
    private String adClickLink;
    @SerializedName("package")
    private String _package;
    @SerializedName("type")
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdImgLink() {
        return adImgLink;
    }

    public void setAdImgLink(String adImgLink) {
        this.adImgLink = adImgLink;
    }

    public String getAdClickLink() {
        return adClickLink;
    }

    public void setAdClickLink(String adClickLink) {
        this.adClickLink = adClickLink;
    }

    public String getPackage() {
        return _package;
    }

    public void setPackage(String _package) {
        this._package = _package;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
