package com.sukraapps.kumaoni.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ravi on 12-Sep-17.
 */

public class Category {
    private int id;
    @SerializedName("name")
    private String title;
    boolean isSelected;
    ArrayList<SubCategory> subcategories;

    public Category(int id,String title, ArrayList<SubCategory> subCategories){
        this.id = id;
        this.title = title;
        this.subcategories = subCategories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ArrayList<SubCategory> getSubCategories() {
        return subcategories;
    }

    public void setSubcategories(ArrayList<SubCategory> subcategories) {
        this.subcategories = subcategories;
    }
}
