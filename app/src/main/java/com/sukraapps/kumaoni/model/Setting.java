package com.sukraapps.kumaoni.model;

/**
 * Created by Ravi on 08-Oct-17.
 */

public class Setting {
    private String youtube_api_key;
    private String exit_image_link;
    private String exit_image_click_link;
    private int ads_visibility;

    private String local_ad_img_link;
    private String local_ad_click_link;
    private String local_ad_img_type;
    private int local_ad_frequency;

    public String getYoutube_api_key() {
        return youtube_api_key;
    }

    public void setYoutube_api_key(String youtube_api_key) {
        this.youtube_api_key = youtube_api_key;
    }

    public String getExit_image_link() {
        return exit_image_link;
    }

    public void setExit_image_link(String exit_image_link) {
        this.exit_image_link = exit_image_link;
    }

    public String getExit_image_click_link() {
        return exit_image_click_link;
    }

    public void setExit_image_click_link(String exit_image_click_link) {
        this.exit_image_click_link = exit_image_click_link;
    }

    public int isAds_visibility() {
        return ads_visibility;
    }

    public void setAds_visibility(int ads_visibility) {
        this.ads_visibility = ads_visibility;
    }

    public int getAds_visibility() {
        return ads_visibility;
    }

    public String getLocal_ad_img_link() {
        return local_ad_img_link;
    }

    public void setLocal_ad_img_link(String local_ad_img_link) {
        this.local_ad_img_link = local_ad_img_link;
    }

    public String getLocal_ad_click_link() {
        return local_ad_click_link;
    }

    public void setLocal_ad_click_link(String local_ad_click_link) {
        this.local_ad_click_link = local_ad_click_link;
    }

    public int getLocal_ad_frequency() {
        return local_ad_frequency;
    }

    public void setLocal_ad_frequency(int local_ad_frequency) {
        this.local_ad_frequency = local_ad_frequency;
    }

    public String getLocal_ad_img_type() {
        return local_ad_img_type;
    }

    public void setLocal_ad_img_type(String local_ad_img_type) {
        this.local_ad_img_type = local_ad_img_type;
    }
}
