package com.sukraapps.kumaoni.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ravi on 13-Sep-17.
 */

public class Video implements Serializable{
    @SerializedName("id")
    private String id;
    @SerializedName("cat_id")
    private int categoryId;
    @SerializedName("subcat_id")
    private int subCategoryId;
    @SerializedName("name")
    private String title;
    @SerializedName("video")
    private String videoLink;
    @SerializedName("ytcode")
    private String videoCode;
    @SerializedName("descr")
    private String description;
    @SerializedName("is_view")
    private String views;

    private int is_audio;

    @SerializedName("ytCode")
    private  String ytcode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getVideoCode() {
        return videoCode;
    }

    public void setVideoCode(String videoCode) {
        this.videoCode = videoCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getYtcode() {
        return ytcode;
    }

    public void setYtcode(String ytcode) {
        this.ytcode = ytcode;
    }

    public int getIs_audio() {
        return is_audio;
    }

    public void setIs_audio(int is_audio) {
        this.is_audio = is_audio;
    }

    public int getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        this.subCategoryId = subCategoryId;
    }
}
