package com.sukraapps.kumaoni.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Ravi on 07-Oct-17.
 */

public class Comment {
    private String userid;
    private String username;
    private String comment;
    @SerializedName("timestamp")
    private Date datetime;

    public Comment(String comment,Date date){
        this.comment = comment;
        this.datetime = date;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
}
