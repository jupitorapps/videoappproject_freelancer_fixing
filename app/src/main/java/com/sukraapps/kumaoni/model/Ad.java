package com.sukraapps.kumaoni.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ravi on 04-Jan-18.
 */

public class Ad {
    @SerializedName("ads")
    private Ads ads;
    @SerializedName("backads")
    private BackAds backads;
    @SerializedName("youtubekey")
    private String youtubeKey;

    public Ads getAds() {
        return ads;
    }

    public void setAds(Ads ads) {
        this.ads = ads;
    }

    public BackAds getBackads() {
        return backads;
    }

    public void setBackads(BackAds backads) {
        this.backads = backads;
    }

    public String getYoutubeKey() {
        return youtubeKey;
    }

    public void setYoutubeKey(String youtubeKey) {
        this.youtubeKey = youtubeKey;
    }
}
