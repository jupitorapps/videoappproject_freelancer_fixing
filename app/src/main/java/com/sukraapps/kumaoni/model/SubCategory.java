package com.sukraapps.kumaoni.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ravi on 04-Oct-17.
 */

public class SubCategory {
    private int id;
    @SerializedName("name")
    private String title;
    private int categoryid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }
}
