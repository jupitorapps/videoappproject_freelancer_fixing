package com.sukraapps.kumaoni.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sukraapps.kumaoni.R;
import com.sukraapps.kumaoni.VideoPlayActivity;
import com.sukraapps.kumaoni.adapter.ClickListener;
import com.sukraapps.kumaoni.adapter.DividerItemDecoration2;
import com.sukraapps.kumaoni.adapter.SupreRecyclerTouchListener;
import com.sukraapps.kumaoni.adapter.VideoAdapter;
import com.sukraapps.kumaoni.model.Video;
import com.sukraapps.kumaoni.service.ApiClient;
import com.sukraapps.kumaoni.service.ApiService;
import com.sukraapps.kumaoni.util.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ravi on 04-Oct-17.
 */

public class FavouriteFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    ApiService apiService = null;
    ArrayList<Video> videoArraylist = null;
    SuperRecyclerView recyclerView = null;
    VideoAdapter adapter = null;

    public static FavouriteFragment newInstance() {
        FavouriteFragment fragmentAction = new FavouriteFragment();
        return fragmentAction;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.default_fragment, container, false);

        recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration2(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new SupreRecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent iv = new Intent(getContext(),VideoPlayActivity.class);
                iv.putExtra("Video",videoArraylist.get(position));
                startActivity(iv);

            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
        recyclerView.setRefreshListener(this);
        recyclerView.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);

        getRecyclerviewData();

        return view;
    }

    private void getRecyclerviewData() {
        if (apiService == null) {
            apiService = ApiClient.getClient().create(ApiService.class);
        }
        Call<ArrayList<Video>> call = apiService.getFavouriteVideos(Util.getUserId(getContext()), getContext().getPackageName());
        call.enqueue(new Callback<ArrayList<Video>>() {
            @Override
            public void onResponse(Call<ArrayList<Video>> call, Response<ArrayList<Video>> response) {
                videoArraylist = response.body();
                if (videoArraylist != null) {
                    adapter = new VideoAdapter(getContext(), videoArraylist);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Video>> call, Throwable t) {
                Toast.makeText(getContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        getRecyclerviewData();
    }
}
