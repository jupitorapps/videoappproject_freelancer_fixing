package com.sukraapps.kumaoni.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sukraapps.kumaoni.MainActivity;
import com.sukraapps.kumaoni.R;
import com.sukraapps.kumaoni.VideoPlayActivity;
import com.sukraapps.kumaoni.adapter.ClickListener;
import com.sukraapps.kumaoni.adapter.DividerItemDecoration2;
import com.sukraapps.kumaoni.adapter.SupreRecyclerTouchListener;
import com.sukraapps.kumaoni.adapter.VideoAdapter;
import com.sukraapps.kumaoni.model.Video;
import com.sukraapps.kumaoni.service.ApiClient;
import com.sukraapps.kumaoni.service.ApiService;
import com.sukraapps.kumaoni.util.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    private static final String KEY_ID = "key_id";
    private static final String KEY_IS_CATEGORY = "key_is_category";

    private static final int HOME_VIDEOS = 1000;
    private static final int HD = 2001;
    private static final int TRENDING = 2002;
    private static final int LATEST = 2009;
    private static final int HISTORY = 2003;
    public static final int SEARCH = 2004;

    int id;
    boolean isCategory;

    int offset = 0;
    ApiService apiService = null;
    ArrayList<Video> videoArraylist = null;
    SuperRecyclerView recyclerView = null;
    VideoAdapter adapter = null;

    public static Video currentVideo = null;
    public static int currentOffset = 0;

    public static HomeFragment newInstance(int id, boolean isCategory) {
        HomeFragment fragmentAction = new HomeFragment();
        Bundle args = new Bundle();

        args.putInt(KEY_ID, id);
        args.putBoolean(KEY_IS_CATEGORY, isCategory);

        fragmentAction.setArguments(args);
        return fragmentAction;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.default_fragment, container, false);
        try{
            id = getArguments().getInt(KEY_ID);
            isCategory = getArguments().getBoolean(KEY_IS_CATEGORY);

            recyclerView = (SuperRecyclerView) view.findViewById(R.id.recyclerView);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setRefreshListener(this);
            recyclerView.setupMoreListener(this, 1);
            recyclerView.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
            recyclerView.addItemDecoration(new DividerItemDecoration2(getContext(), LinearLayoutManager.VERTICAL));

            recyclerView.addOnItemTouchListener(new SupreRecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    try{
                        Video current = videoArraylist.get(position);
                        if (current == null){
                            Toast.makeText(getContext(),"Video not found. It might have been deleted",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        currentVideo = current;
                        currentOffset = offset;

                        if (MainActivity.mInterstitialAd2 != null && MainActivity.mInterstitialAd2.isLoaded()){
                            MainActivity.mInterstitialAd2.show();
                        }else{
                            Intent iv = new Intent(getContext(),VideoPlayActivity.class);
                            iv.putExtra("Video",current);
                            iv.putExtra("offset",offset);
                            startActivity(iv);
                            MainActivity.mInterstitialAd2.loadAd(new AdRequest.Builder().build());
                        }
                    }catch (Exception e){
                        Toast.makeText(getContext(),"Video not found. It might have been deleted",Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));

            getRecyclerviewData();
        }catch (Exception e){}

        return view;
    }

    private void getRecyclerviewData() {
        offset = 0;

        Call<ArrayList<Video>> call;
        if (apiService == null) {
            apiService = ApiClient.getClient().create(ApiService.class);
        }

        if (id == HOME_VIDEOS) {
            call = apiService.getHomeVideos(Util.getUserId(getContext()),offset,MainActivity.filterKeyword);
        }
        else if(id == HD){
            call = apiService.getHDVideos(offset);
        }
        else if(id == LATEST){
            call = apiService.getLatestVideos(offset,MainActivity.filterKeyword);
        }
        else if(id == TRENDING){
            call = apiService.getTrendingVideos(offset,MainActivity.filterKeyword);
        }
        else if(id == HISTORY){
            call = apiService.getHistoryVideos(Util.getUserId(getContext()),offset,getContext().getPackageName());
        }
        else if(id == SEARCH){
            call = apiService.getSearchVideos(Util.getUserId(getContext()), MainActivity.searchKeyword,offset);
        }
        else {
            if (isCategory){
                // for Category
                call = apiService.getHomeVideosByCategoryId(offset, id,MainActivity.filterKeyword);
            }else{
                // for SubCategory
                call = apiService.getHomeVideosBySubCategoryId(offset, id,MainActivity.filterKeyword);
            }

        }

        call.enqueue(new Callback<ArrayList<Video>>() {
            @Override
            public void onResponse(Call<ArrayList<Video>> call, Response<ArrayList<Video>> response) {

                videoArraylist = response.body();

                if (videoArraylist != null) {
                    offset += videoArraylist.size();
                    adapter = new VideoAdapter(getContext(), videoArraylist);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Video>> call, Throwable t) {
                Toast.makeText(getContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        getRecyclerviewData();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        if (apiService == null) {
            apiService = ApiClient.getClient().create(ApiService.class);
        }

        Call<ArrayList<Video>> call;
        if (apiService == null) {
            apiService = ApiClient.getClient().create(ApiService.class);
        }

        if (id == HOME_VIDEOS) {
            call = apiService.getHomeVideos(Util.getUserId(getContext()),offset,MainActivity.filterKeyword);
        }
        else if(id == HD){
            call = apiService.getHDVideos(offset);
        }
        else if(id == TRENDING){
            call = apiService.getTrendingVideos(offset,MainActivity.filterKeyword);
        }
        else if(id == LATEST){
            call = apiService.getLatestVideos(offset,MainActivity.filterKeyword);
        }
        else if(id == HISTORY){
            call = apiService.getHistoryVideos(Util.getUserId(getContext()),offset,getContext().getPackageName());
        }
        else if(id == SEARCH){
            call = apiService.getSearchVideos(Util.getUserId(getContext()), MainActivity.searchKeyword,offset);
        }
        else {
            if (isCategory){
                // for Category
                call = apiService.getHomeVideosByCategoryId(offset, id,MainActivity.filterKeyword);
            }else{
                // for SubCategory
                call = apiService.getHomeVideosBySubCategoryId(offset, id,MainActivity.filterKeyword);
            }

        }

        call.enqueue(new Callback<ArrayList<Video>>() {
            @Override
            public void onResponse(Call<ArrayList<Video>> call, Response<ArrayList<Video>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {
                        offset += response.body().size();
                        adapter.add(response.body());
                    } else {
                        recyclerView.removeMoreListener();
                        recyclerView.hideMoreProgress();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Video>> call, Throwable t) {
                Toast.makeText(getContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
}