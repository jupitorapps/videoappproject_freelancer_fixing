package com.sukraapps.kumaoni;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.sukraapps.kumaoni.adapter.CustomExpandableListAdapter;
import com.sukraapps.kumaoni.fragment.HomeFragment;
import com.sukraapps.kumaoni.model.Ad;
import com.sukraapps.kumaoni.model.Category;
import com.sukraapps.kumaoni.model.SubCategory;
import com.sukraapps.kumaoni.navigation.FragmentNavigationManager;
import com.sukraapps.kumaoni.navigation.NavigationManager;
import com.sukraapps.kumaoni.service.AdClient;
import com.sukraapps.kumaoni.service.ApiClient;
import com.sukraapps.kumaoni.service.ApiService;
import com.sukraapps.kumaoni.util.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ExpandableListView mExpandableListView;
    private ExpandableListAdapter mExpandableListAdapter;
    private ArrayList<Category> categoryArrayList = null;

    private NavigationManager mNavigationManager = null;
    private DrawerLayout mDrawerLayout = null;
    private ActionBarDrawerToggle mDrawerToggle = null;

    public static String searchKeyword = "";
    public static String filterKeyword = "All";
    public static boolean shouldShowAds = true;
    public static boolean shouldShowRateDialog = false;

    // For filteres. When filter clicks we need to update UI based on filter.
    public String title_filter;
    public int id_filter = 1000;
    public boolean is_category_filter = true;
    boolean isBack = false;

    public static InterstitialAd mInterstitialAd2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpUI();
        setUpRecyclerViewData();

        if (mNavigationManager != null) {
            mNavigationManager.defaultFragment(1000, true);
        }

        shouldShowAds = Util.shouldShowAds(this);
        shouldShowRateDialog = Util.shouldShowRateDialog(this);

        if (shouldShowAds) {
            loadAdmobAd2();
        }

        isStoragePermissionGranted();
        checkUserId();
        if (shouldShowRateDialog) {
            showRateDialog();
        }

        setupAds();
    }

    private void loadAdmobAd2() {
        mInterstitialAd2 = new InterstitialAd(this);
        mInterstitialAd2.setAdUnitId(getResources().getString(R.string.AdmobFullScreenAdsID2));
        mInterstitialAd2.loadAd(new AdRequest.Builder().build());

        mInterstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
                try {
                    if (isBack){
                        //finish();
                        showExitAlert();
                    }else{
                        mInterstitialAd2.loadAd(new AdRequest.Builder().build());
                        Intent iv = new Intent(getApplicationContext(), VideoPlayActivity.class);
                        iv.putExtra("Video", HomeFragment.currentVideo);
                        iv.putExtra("offset", HomeFragment.currentOffset);
                        startActivity(iv);
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    private void showExitAlert(){ 
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


    private void setUpUI() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mNavigationManager = FragmentNavigationManager.obtain(this);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        categoryArrayList = new ArrayList<>();
        title_filter = getString(R.string.app_name);
    }

    private void setUpRecyclerViewData() {
        mExpandableListView = (ExpandableListView) findViewById(R.id.navList);
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<ArrayList<Category>> call = apiService.getCategories();
        call.enqueue(new Callback<ArrayList<Category>>() {
            @Override
            public void onResponse(Call<ArrayList<Category>> call, Response<ArrayList<Category>> response) {

                categoryArrayList = response.body();
                if (categoryArrayList != null) {

                    Category c = new Category(1000, getResources().getString(R.string.Home), null);
                    categoryArrayList.add(0, c);
                    //categoryArrayList.add(1,new Category(2001, "HD", null));
                    categoryArrayList.add(1, new Category(2002, getResources().getString(R.string.Trending), null));
                    categoryArrayList.add(2, new Category(2009, getResources().getString(R.string.Latest), null));
                    categoryArrayList.add(3, new Category(1099, getResources().getString(R.string.Favourite), null));
                    categoryArrayList.add(4, new Category(2003, getResources().getString(R.string.History), null));
                    // Category c2 = new Category(1001, getResources().getString(R.string.adfree), null);
                    Category c3 = new Category(1002, getResources().getString(R.string.rateapp), null);
                    Category c4 = new Category(1003, getResources().getString(R.string.shareapp), null);
                    Category c5 = new Category(1004, getResources().getString(R.string.aboutus), null);
                    // categoryArrayList.add(c2);
                    categoryArrayList.add(c3);
                    categoryArrayList.add(c4);
                    categoryArrayList.add(c5);

                    mExpandableListAdapter = new CustomExpandableListAdapter(getApplicationContext(), categoryArrayList);
                    mExpandableListView.setAdapter(mExpandableListAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Category>> call, Throwable t) {
                Toast.makeText(MainActivity.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            }
        });

        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                Category currentCategory = categoryArrayList.get(groupPosition);

                //for Home
                if (currentCategory.getId() == 1000) {
                    id_filter = currentCategory.getId();
                    title_filter = getString(R.string.app_name);
                    is_category_filter = true;

                    mNavigationManager.defaultFragment(1000, true);
                    getSupportActionBar().setTitle(getString(R.string.app_name));
                }
                //for Make Ad Free
                else if (currentCategory.getId() == 1001) {
                    Intent i = new Intent(getApplicationContext(), MakeAdFreeActivity.class);
                    startActivity(i);
                }
                // for Rate App
                else if (currentCategory.getId() == 1002) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                    }
                }
                // for Share app
                else if (currentCategory.getId() == 1003) {
                    try {
                        String sAux = "\n" + getResources().getString(R.string.share_description) + "\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();

                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.shareimage);
                        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/LatestShare.jpg";
                        OutputStream out = null;
                        File file = new File(path);
                        try {
                            out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                            out.flush();
                            out.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        path = file.getPath();
                        Uri bmpUri = Uri.parse("file://" + path);


                        Intent intent = new Intent();
                        intent.putExtra(Intent.EXTRA_TEXT, sAux);
                        intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        intent.setType("image/*");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(Intent.createChooser(intent, "Select app"));
                    } catch (Exception e) {

                    }
                }
                // for about us
                else if (currentCategory.getId() == 1004) {
                    Intent i = new Intent(getApplicationContext(), AboutUsActivity.class);
                    startActivity(i);
                }
                // for favourite
                else if (currentCategory.getId() == 1099) {
                    mNavigationManager.favouriteFragment();
                    getSupportActionBar().setTitle("Favourite");
                } else {
                    // For Category Click
                    id_filter = currentCategory.getId();
                    title_filter = currentCategory.getTitle();
                    is_category_filter = true;

                    mNavigationManager.defaultFragment(currentCategory.getId(), true);
                    getSupportActionBar().setTitle(currentCategory.getTitle());
                }

                /*if (shouldShowAds && mInterstitialAd2 != null && mInterstitialAd2.isLoaded() && currentCategory.getId() != 1002 && currentCategory.getId() != 1003 && currentCategory.getId() != 1004) {
                    mInterstitialAd2.show();
                }else{
                    if(mInterstitialAd2 != null && !mInterstitialAd2.isLoaded()){
                        mInterstitialAd2.loadAd(new AdRequest.Builder().build());
                    }
                }*/

                return true; // This way the expander cannot be collapsed
            }
        });

        mExpandableListView.setOnChildClickListener(
                new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v,
                                                int groupPosition, int childPosition, long id) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        SubCategory selecteditem = categoryArrayList.get(groupPosition).getSubCategories().get(childPosition);

                        id_filter = selecteditem.getId();
                        title_filter = selecteditem.getTitle();
                        is_category_filter = false;

                        mNavigationManager.defaultFragment(selecteditem.getId(), false);
                        getSupportActionBar().setTitle(selecteditem.getTitle());

                        /*if (shouldShowAds && mInterstitialAd2 != null && mInterstitialAd2.isLoaded()) {
                            mInterstitialAd2.show();
                        }else{
                            if(mInterstitialAd2 != null && !mInterstitialAd2.isLoaded()){
                                mInterstitialAd2.loadAd(new AdRequest.Builder().build());
                            }
                        }*/

                        return false;
                    }
                });

    }

    @Override
    public void onBackPressed() {

        // if drawer open then close drawer
        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        } else {
            if (ad != null && ad.getBackads() != null && !ad.getBackads().getAdImgLink().isEmpty()){
                showExitDialog(this);
            }else{
                isBack=true;
                if (mInterstitialAd2 != null && mInterstitialAd2.isLoaded()){
                    mInterstitialAd2.show();
                }else{

                    showExitAlert();
                   // finish();
                }
            }
        }

    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    private void checkUserId() {
        if (Util.getUserId(this).isEmpty()) {
            String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            ApiService apiService = ApiClient.getClient().create(ApiService.class);
            Call<String> call = apiService.getUserId(android_id);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    String userid = response.body();
                    Util.saveUserId(getApplicationContext(), userid);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    //Toast.makeText(MainActivity.this, getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void isStoragePermissionGranted() {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {

                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                }
            }
        } catch (Exception e) {
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchmenu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    return true;
                }
            });

            EditText searchPlate = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchPlate.setHint("Search");
            View searchPlateView = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            searchPlateView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
            // use this method for search process
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(final String query) {
                    // use this method when query submitted
                    if (query.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please write something...", Toast.LENGTH_SHORT).show();
                    } else {
                        searchKeyword = query;
                        getSupportActionBar().setTitle("Search results");
                        mNavigationManager.defaultFragment(HomeFragment.SEARCH, true);
                    }

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    // use this method for auto complete search process
                    return false;
                }
            });

            MenuItemCompat.setOnActionExpandListener(searchItem,
                    new MenuItemCompat.OnActionExpandListener() {
                        @Override
                        public boolean onMenuItemActionCollapse(MenuItem item) {
                            // Do something when collapsed
                            //mNavigationManager.defaultFragment();
                            return true; // Return true to collapse action view
                        }

                        @Override
                        public boolean onMenuItemActionExpand(MenuItem item) {
                            // Do something when expanded
                            return true; // Return true to expand action view
                        }
                    });


            SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (item.getItemId() == R.id.action_search) {
            return super.onOptionsItemSelected(item);
        }

        int id = item.getItemId();
        if (item.isChecked())
            item.setChecked(false);
        else
            item.setChecked(true);

        switch (id) {
            case R.id.allvideo:
                filterKeyword = "All";
                break;
            case R.id.hdonly:
                filterKeyword = "HD";
                break;
            case R.id.audioonly:
                filterKeyword = "Audio";
                break;
            default:
                break;
        }
        mNavigationManager.defaultFragment(id_filter, is_category_filter);
        getSupportActionBar().setTitle(title_filter);

        return super.onOptionsItemSelected(item);
    }

    private void showRateDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.rate_dialog);

        final EditText suggestionET = (EditText) dialog.findViewById(R.id.suggestion);

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating >= 4) {
                    suggestionET.setVisibility(View.GONE);
                }
            }
        });

        final TextView submitTV = (TextView) dialog.findViewById(R.id.submit);
        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rating = ratingBar.getRating();
                if (rating >= 4) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                    }
                    Util.saveAppRatedToTrue(getApplicationContext());
                    dialog.dismiss();
                } else {
                    suggestionET.setVisibility(View.VISIBLE);
                    String suggestion = suggestionET.getText().toString();
                    if (suggestion.isEmpty()) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.feedbackerror), Toast.LENGTH_LONG).show();
                    } else {
                        saveReview(suggestion);
                        Util.saveAppRatedToTrue(getApplicationContext());
                        dialog.dismiss();
                    }
                }
            }
        });

        dialog.show();
    }

    private void saveReview(String review) {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<String> call = apiService.saveReview(Util.getUserId(this), review);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.feedback_text), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        try {
            switch (requestCode) {
                case 1:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        saveCloseDialogImageToSdCard();
                    } else {
                        // Permission Denied
                        Toast.makeText(MainActivity.this, "WRITE_STORAGE Permission Denied", Toast.LENGTH_SHORT)
                                .show();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } catch (Exception e) {
        }
    }

    public void showExitDialog(final Activity activity){
        try{
            updateBackAdsSeen();
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            LayoutInflater factory = LayoutInflater.from(activity);
            final View view = factory.inflate(R.layout.imagedialouge, null);
            ImageView i = (ImageView) view.findViewById(R.id.dialog_imageview);
            Button exit = (Button) view.findViewById(R.id.exitBtn);
            Button stay = (Button) view.findViewById(R.id.stayBtn);

            try{
                String name = ad.getBackads().getAdImgLink().substring(ad.getBackads().getAdImgLink().lastIndexOf('/') + 1);
                String completePath = Environment.getExternalStorageDirectory().toString() + "/"+getString(R.string.app_name)+"/"+name;
                File file = new File(completePath);
                Uri imageUri = Uri.fromFile(file);
                Glide.with(activity)
                        .load(imageUri)
                        .placeholder(R.drawable.loading )
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(i);
            }catch (Exception e){}

            i.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        String strUrl = ad.getBackads().getAdClickLink();
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(strUrl)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                    }catch(Exception e){}
                }
            });

            builder.setView(view);
            builder.setCancelable(false);

            final AlertDialog alert = builder.create();

            stay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.cancel();
                }
            });

            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finishAffinity();
                }
            });

            alert.show();
        }catch (Exception e){}
    }

    private Ad ad;
    private void setupAds(){
        Util.updateAppOpenCount(getApplicationContext());
        String userid = Util.getUserId(getApplicationContext());
        if (userid.isEmpty()){
            userid = "0";
        }
        ApiService apiService = AdClient.getClient().create(ApiService.class);
        Call<Ad> call = apiService.getAds(userid,getPackageName());
        call.enqueue(new Callback<Ad>() {
            @Override
            public void onResponse(Call<Ad> call, Response<Ad> response) {
                if (response.body() != null){
                    ad = response.body();
                    if (ad != null && ad.getYoutubeKey() != null && !ad.getYoutubeKey().isEmpty()){
                        Util.saveYoutubeApiKey(getApplicationContext(),ad.getYoutubeKey());
                    }
                    if (ad != null && ad.getBackads() != null && !ad.getBackads().getAdImgLink().isEmpty()){
                        saveCloseDialogImageToSdCard();
                    }
                    if (ad != null && ad.getAds() != null && !ad.getAds().getAdImgLink().isEmpty() && Util.getAppOpenCount(getApplicationContext()) % 3 == 0){
                        showLocalAdsDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Ad> call, Throwable t) {

            }
        });
    }

    private void saveCloseDialogImageToSdCard(){
        try{
            if (ad != null && ad.getBackads() != null && !ad.getBackads().getAdImgLink().isEmpty())
                Glide.with(this).load(ad.getBackads().getAdImgLink())
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL,Target.SIZE_ORIGINAL) {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                                try {
                                    String root = Environment.getExternalStorageDirectory().toString();
                                    File myDir = new File(root + "/"+getString(R.string.app_name));

                                    if (!myDir.exists()) {
                                        myDir.mkdirs();
                                    }

                                    String name = ad.getBackads().getAdImgLink().substring(ad.getBackads().getAdImgLink().lastIndexOf('/') + 1);

                                    myDir = new File(myDir, name);
                                    FileOutputStream out = new FileOutputStream(myDir);
                                    resource.compress(Bitmap.CompressFormat.JPEG, 90, out);

                                    out.flush();
                                    out.close();
                                } catch(Exception e){
                                    // some action
                                }
                            }
                        });
        }catch (Exception e){}
    }

    private void showLocalAdsDialog(){
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater factory = LayoutInflater.from(this);
            final View view = factory.inflate(R.layout.dialog_local_ad, null);
            final ImageView i = (ImageView) view.findViewById(R.id.imageView);
            ImageView exit = (ImageView) view.findViewById(R.id.close);

            builder.setView(view);
            builder.setCancelable(false);

            final AlertDialog alert = builder.create();

            i.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String type = ad.getAds().getType();
                    String strUrl = ad.getAds().getAdClickLink();
                    try {
                        if(type.equals("web") || type.equals("browser")){
                            alert.dismiss();
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(strUrl)));
                        }else if(type.equals("play")){
                            try {
                                alert.dismiss();
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + strUrl)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + strUrl)));
                            }
                        }else{
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(strUrl)));
                        }
                    } catch (android.content.ActivityNotFoundException anfe) {
                    }catch(Exception e){}
                }
            });

            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });

            Glide.with(this).load(ad.getAds().getAdImgLink())
                    .asBitmap()
                    .into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                            try {
                                i.setImageBitmap(resource);
                                alert.show();
                                updateAdsSeen();
                            } catch(Exception e){
                                // some action
                            }
                        }
                    });
        }catch (Exception e){}
    }

    private void updateBackAdsSeen(){
        ApiService apiService = AdClient.getClient().create(ApiService.class);
        Call<ResponseBody> call = apiService.updateBackAdsSeen(Util.getUserId(getApplicationContext()),getPackageName(),ad.getBackads().getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void updateAdsSeen(){
        ApiService apiService = AdClient.getClient().create(ApiService.class);
        Call<ResponseBody> call = apiService.updateAdsSeen(Util.getUserId(getApplicationContext()),getPackageName(),ad.getAds().getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}
